package com.example.demo.Visual;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Edgar Gómez on 15/07/2017.
 */
public interface MyRepository extends JpaRepository<Estudiante, Long> {
}
